<?php

class Database {

    // Dados de acesso ao banco de dados MySQL
    private $host       = 'localhost';
    private $usuario    = 'root';
    private $senha      = 'pwd';
    private $baseMySQL  = 'mysql_db';

    // Dados de acesso ao banco de dados MongoDB
    private $baseMongoDB = 'mongodb_db';

    /**
    * Conecta e retorna conexão com banco de dados MySQL
    */
    protected function conectarMySQL() {
        try {
            $conexao = new PDO('mysql:host='+$this->host+';dbname='+$this->baseMySQL, $this->usuario, $this->senha);
            return $conexao;
        } catch (PDOException $e) {
            print "Erro ao tentar conexão com banco de dados MySQL: " . $e->getMessage();
            die();
        }
    }

    /**
    * Conecta e retorna conexão com banco de dados MongoDB
    */
    protected function conectarMongoDB() {
        $mongoDB = new Mongo();
        $conexao = $mongoDB->selectDB($this->baseMongoDB);

        return $this->conexao;
    }

}

?>
