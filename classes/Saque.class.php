<?php

include_once('exceptions/NoteUnavailableException.php');

/**
 * Classe para saque de valores no caixa eletrônico
 *
 * @author          Lucas Bento <lucas.bsilva@outlook.com>
 */

class Saque {

    // Notas disponíveis no caixa eletrônico
    private $notas = array(100, 50, 20, 10);

    /*
     * Função para sacar valores do caixa eletrônico.
     * 
     * @param int $valor Valor solicitado ao caixa eletrônico.
     * @return array $entregaNotas Notas à serem entregues ao solicitante.
     */
    public function sacar($valor) {
        if (($valor % 2) > 0) {
            throw new NoteUnavailableException();
        }

        $notas = $this->notas; // Notas disponíveis para a quantia
        $faltante = $valor; // Valor faltante para entrega de toda a quantia
        $entregaNotas = []; // Notas à serem entregas
        // Laço de repetição que termina apenas quando o valor total de notas for igual ao valor solicitado
        while (array_sum($entregaNotas) < $valor):
            $maiorValor = max($notas);

            if ($faltante < $maiorValor) {
                // Remove a nota caso o valor dela seja maior que o faltante
                $notas = array_diff($notas, array($maiorValor));
            } else {
                // Adiciona a nota às notas à serem entregues
                $faltante = $faltante - $maiorValor;
                
                array_push($entregaNotas, $maiorValor);
            }
        endwhile;

        return $entregaNotas;
    }

}

?>