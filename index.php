<html>
    <head>
        <meta charset="UTF-8">
        <title>Caixa Eletrônico</title>
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    </head>
    <body>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-md-offset-3">
            <h3 class="text-center">Saque - Caixa Eletrônico</h3>
            <div id="saque">
                <form id="formSacar" action="saque.php" method="POST">
                    <div class="form-group">
                        <label class="sr-only" for="valor">Valor (em reais)</label>
                        <div class="input-group">
                            <div class="input-group-addon">R$</div>
                            <input type="text" class="form-control" id="valor" name="valor" placeholder="Valor">
                            <div class="input-group-addon">,00</div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Sacar</button>
                </form>
            </div>
            <div id="saqueNotas" style="display: none;">
                <table id="notas" class="table table-bordered table-hover">
                    <thead>
                    <th>
                        <i class="glyphicon glyphicon-usd text-success"></i> Nota(s) Entregue(s)
                    </th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="pull-left">
                    <h4 id="qtdNotas">Número de Notas: <strong class="text-primary">0</strong>
                    </h4>
                </div>
                <button type="button" class="btn btn-default pull-right">Voltar</button>
            </div>
        </div>
        <script type="text/javascript" src="assets/js/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            var saque = $('#saque');
            var saqueNotas = $('#saqueNotas');
            var qtdNotas = $('#qtdNotas strong');

            saqueNotas.find('button').click(function () {
                $(this).parent().fadeOut('slow', function () {
                    saque.fadeIn('slow');
                });
            });

            $('#formSacar').submit(function (e) {
                e.preventDefault();

                $.ajax({
                    url: $(this).attr('action'),
                    type: $(this).attr('method'),
                    async: true,
                    data: $(this).serialize(),
                    dataType: 'JSON',
                    success: function (notas) {
                        qtdNotas.text(notas.length);

                        var tabela = $('#notas tbody:last');
                        tabela.find('tr').remove();
                        $.each(notas, function (k, nota) {
                            nota = 'R$ ' + nota + ',00';
                            tabela.append('<tr><td>' + nota + '</td></tr>');
                        });

                        saque.fadeOut('slow', function () {
                            saqueNotas.fadeIn('slow');
                        });
                    }
                });
            });
        </script>
    </body>
</html>
